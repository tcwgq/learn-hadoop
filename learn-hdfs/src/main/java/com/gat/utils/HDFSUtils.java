package com.gat.utils;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

public class HDFSUtils {
	private static final String HDFS_URL = "hdfs://10.101.9.197:8020";
	private static Configuration conf = null;
	private static FileSystem fs = null;
	static {
		conf = new Configuration();
		conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
		// conf.set("user.name", "wgq");
		try {
			fs = FileSystem.get(URI.create(HDFS_URL), conf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void createFile(String path, byte[] bytes) {
		Path destPath = new Path(path);
		try {
			FSDataOutputStream fsos = fs.create(destPath);
			fsos.write(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void putFile(String srcFile, String destFile) {
		Path srcPath = new Path(srcFile);
		Path destPath = new Path(destFile);
		try {
			// true为删除原文件，false为保留原文件
			fs.copyFromLocalFile(false, srcPath, destPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void listFiles(String destFile) {
		try {
			RemoteIterator<LocatedFileStatus> it = fs.listFiles(new Path(destFile), true);
			while (it.hasNext()) {
				LocatedFileStatus status = it.next();
				System.out.println(status.getPath().getName());
			}
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void listFileStatuses(String destFile) {
		try {
			FileStatus[] listStatus = fs.listStatus(new Path(destFile));
			for (FileStatus status : listStatus) {
				System.out.println(status.toString());
			}
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void deleteFile(String destFile) {
		try {
			boolean result = fs.delete(new Path(destFile), true);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void mkdirs(String destFile) {
		try {
			boolean result = fs.mkdirs(new Path(destFile));
			System.out.println(result);
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
	}
}
