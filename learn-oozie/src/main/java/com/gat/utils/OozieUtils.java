package com.gat.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.OozieClientException;
import org.apache.oozie.client.WorkflowAction;
import org.apache.oozie.client.WorkflowJob;
import org.apache.oozie.client.WorkflowJob.Status;

/**
 * 操作oozie 工作流的工具类 。 注意：配置文件确保放在类路径下，程序默认读取类路径下的配置文件。
 * 
 * @author wgq 2017/02/23
 *
 */
public class OozieUtils {
	private static String OOZIE_URL = "http://10.101.9.198:11000/oozie";
	private static OozieClient client = new OozieClient(OOZIE_URL);

	public static String startJob(String filePath) {
		if (StringUtils.isEmpty(filePath) && StringUtils.isBlank(filePath)) {
			throw new RuntimeException("filePath can't be null or empty!");
		}
		Properties conf = new Properties();
		InputStream is = OozieUtils.class.getClassLoader().getResourceAsStream(filePath);
		try {
			conf.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			String jobId = client.run(conf);
			return jobId;
		} catch (OozieClientException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static String getJobInfo(String jobId) {
		try {
			WorkflowJob workflowJob = client.getJobInfo(jobId);
			System.out.println(workflowJob.getAppName());// 获取应用名称
			System.out.println(workflowJob.getAppPath());// 获取应用路径
			System.out.println(workflowJob.getConf());// 获取配置
			System.out.println(workflowJob.getRun());// 获取运行时的编号
			System.out.println(workflowJob.getUser());// 获取用户名
			System.out.println(workflowJob.getStatus());
			Status status = workflowJob.getStatus();
			System.out.println(status.toString());
			System.out.println(status.toString());
			List<WorkflowAction> actions = workflowJob.getActions();
			System.out.println("-----------actions-------------");
			for (WorkflowAction action : actions) {
				System.out.println(action.getConf());
				System.out.println(action.getData());
				System.out.println(action.getConsoleUrl());
				System.out.println(action.getId());
				System.out.println(action.getName());
				System.out.println(action.getType());
			}
			System.out.println("-----------actions--------------");
			return workflowJob.toString();
		} catch (OozieClientException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void killJob(String jobId) {
		if (StringUtils.isEmpty(jobId) && StringUtils.isBlank(jobId)) {
			return;
		}
		try {
			client.kill(jobId);
		} catch (OozieClientException e) {
			e.printStackTrace();
		}
	}

}
