package com.gat.utils;

import java.io.IOException;
import java.net.URI;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

import com.gat.oozie.WorkflowClient;

public class HDFSUtils {
	private static final String HDFS_URL = "hdfs://10.101.9.197:8020";
	private static Configuration conf = null;
	private static FileSystem fs = null;

	static {
		conf = new Configuration();
		conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
		conf.set("user.name", "wgq");
		try {
			fs = FileSystem.get(URI.create(HDFS_URL), conf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void createFile(String destFile, String content) {
		if (StringUtils.isEmpty(destFile) && StringUtils.isBlank(destFile)) {
			return;
		}
		if (StringUtils.isEmpty(content) && StringUtils.isBlank(content)) {
			return;
		}
		byte[] bytes = content.getBytes();
		Path destPath = new Path(destFile);
		try {
			FSDataOutputStream fsos = fs.create(destPath);
			fsos.write(bytes);
			fsos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void putFile(String srcFile, String destFile) {
		if (StringUtils.isEmpty(srcFile) && StringUtils.isBlank(srcFile)) {
			return;
		}
		if (StringUtils.isEmpty(destFile) && StringUtils.isBlank(destFile)) {
			return;
		}
		Path srcPath = new Path(srcFile);
		Path destPath = new Path(destFile);
		try {
			fs.copyFromLocalFile(false, srcPath, destPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void listFiles(String destFile) {
		if (StringUtils.isEmpty(destFile) && StringUtils.isBlank(destFile)) {
			return;
		}
		try {
			RemoteIterator<LocatedFileStatus> it = fs.listFiles(new Path(destFile), true);
			while (it.hasNext()) {
				LocatedFileStatus status = it.next();
				System.out.println(status.getPath().getName());
			}
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void listFileStatuses(String destFile) {
		if (StringUtils.isEmpty(destFile) && StringUtils.isBlank(destFile)) {
			return;
		}
		try {
			FileStatus[] listStatus = fs.listStatus(new Path(destFile));
			for (FileStatus status : listStatus) {
				System.out.println(status.toString());
			}
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean deleteFile(String destFile, boolean recursive) {
		if (StringUtils.isEmpty(destFile) && StringUtils.isBlank(destFile)) {
			return false;
		}
		try {
			return fs.delete(new Path(destFile), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean mkdirs(String destFile) {
		if (StringUtils.isEmpty(destFile) && StringUtils.isBlank(destFile)) {
			return false;
		}
		try {
			return fs.mkdirs(new Path(destFile));
		} catch (IllegalArgumentException | IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 获取类路径下配置文件的真是路径
	 * 
	 * @param filename
	 * @return
	 */
	public static String getClassPathFile(String filename) {
		if (StringUtils.isEmpty(filename) && StringUtils.isBlank(filename)) {
			return null;
		}
		URL url = WorkflowClient.class.getClassLoader().getResource(filename);
		return url.getPath().substring(1);
	}
}
